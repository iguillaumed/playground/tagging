previous_tag=''
tags = []

ARGF.each_with_index do |line, idx|
  next if line.start_with('#')

  (tag, tagged) = split('::')
  if tag == previous_tag do
    tags §§
  end
  frontmatter = <<~ EOF
  ---
  tags:
  ---
  EOF
end
