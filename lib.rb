# frozen_string_literal: true

require 'json'
require 'mustache'
require 'pp'
require 'yaml'

module Gollum
  class Macro
    # Lists documents tagged by the current (main) page
    # with hyperlinks to the documents' pages.
    class AllPagesTagged < Gollum::Macro
      def render(depth = 4)
        page = @page.parent_page || @page
        STDERR.puts "page: #{page.pretty_inspect}"
        tag = page.name
        render_tagged_tree(tag, depth.to_i)
      end

    private

      def render_tagged_tree(tag, depth)
        STDERR.puts "tag: #{tag}"
        index_path = "/wiki/#{tag}..json"
        STDERR.puts "index_path: #{index_path}"
        begin
          index_JSON = ::File.read(index_path)
        rescue ::Errno::ENOENT
          index_JSON = '{}'
        end
        STDERR.puts "index_JSON: #{index_JSON}"
        index = ::JSON.parse(index_JSON)
        STDERR.puts "index: #{index}"
        unless index.empty?
          return false if depth <= 0

          '<ul class="tagged-pages">' + index.keys.map { |p|
            n = ::File::basename(p, ".md")
            sub = render_tagged_tree(n, depth - 1)
            inner =
              sub ||
              if sub.equal?(false)
                '<span class="more-tagged-pages">&hellip;</span>'
              else
                ''
              end
            '<li>' \
              "<a href='#{::CGI::escape(p).gsub('+', '%20')
                }'>#{::CGI::escapeHTML(n)}</a>" +
              inner +
              '</li>'
          }.join + '</ul>'
        end
      end
    end

    # Lists documents tagging the current (main) page
    # with hyperlinks to the documents' pages.
    class PageTags < Gollum::Macro
      def render
        page = @page.parent_page
        return '[Avoided infinite recursion!]' unless page

        STDERR.puts "page: #{page.pretty_inspect}"
        metadata = page.metadata
        return '' unless metadata

        tags = metadata['tags']
        unless tags
          return '<div id="tags">No tags.</div>'
        end

        'Tags: <ul id="tags">' + tags.map { |t|
          p = t + '.md'
          "<li><a href='#{::CGI::escape(p).gsub('+', '%20')
            }'>#{::CGI::escapeHTML(t)}</a></li>"
        }.join + '</ul>'
      end
    end
  end
end

STDERR.puts "Module.nesting: #{Module.nesting}"

# Holder for misc. stuff.
module GollumTagging
  class Indexer
    def self.index(base_path, name, content)
      puts "name: #{name}"
      puts "content: #{content}"
      if content.equal?(false)
        #TODO: unindex tags of moved/deleted documents
        return
      end

      unless content.is_a? (String)
        subd = name
        content.each { |name, content|
          index(base_path, name, content)
        }
        return
      end

      begin
        return unless content.start_with?('---')

        metadata = YAML.safe_load(content)
        return unless metadata['tags']

        metadata['tags'].each { |tag|
          puts "tag: #{tag}"
          #XXX '..json' is used to avoid file name collisions between indexed
          # and uploaded files:
          index_path = "#{base_path}/#{tag}..json"
          puts "index_path: #{index_path}"
          begin
            old_index_JSON = File.read(index_path)
          rescue Errno::ENOENT
            old_index_JSON = '{}'
          end
          index = JSON.parse(old_index_JSON)
          index[name] = 1  # NOTE: we may need a value someday...
          sorted_index = index.keys.sort.map { |k| [k, index[k]] }.to_h
          index_JSON = JSON.fast_generate(sorted_index)
          next unless index_JSON != old_index_JSON

          File.write(index_path, index_JSON)
        }
      rescue SyntaxError
        STDERR.puts "Error parsing metadata of #{name}\n: #{content}"
      end
    end
  end
  def self.post_commit_hook(committer, sha1)
    system('echo ===================================')
    system('pwd')
    puts committer.index.to_s
    #puts committer.index.pretty_inspect
    #puts committer.index.methods
    #puts committer.index.tree.pretty_inspect
    #puts committer.index.tree.methods
    #TODO: unindex deleted tags
    committer.index.tree.each { |name, content|
      Indexer.index('/wiki', name, content)
    }
    #puts committer.index[0]
    #committer.index.each { |i| puts i.inspect }
    #committer.index.entries.map { |e|
    #  puts e[:path]
    #}
    #puts "...pi: #{committer.index.index.pretty_inspect}"
    #puts "...m: #{committer.index.index.methods}"
    system('echo ===================================')
  end
end

def add_context_aware_content_filter(
  filter_pattern,
  templates_dirname,
  suffix
)
  document_format_suffix = '.md'
  STDERR.puts "templates_dirname: #{templates_dirname.inspect}"
  Gollum::TemplateFilter.add_filter(filter_pattern, & lambda do |page|
    new_page_file_name = page.fullname
    STDERR.puts "new_page_file_name: #{new_page_file_name}"
    repo = page.wiki.repo
    #FIXME??? may be nil???
    head_ref = repo.head
    #STDERR.puts "head_ref: #{head_ref.inspect}"
    #FIXME may be nil
    head_commit = head_ref.commit
    #STDERR.puts "head_commit: #{head_commit.inspect}"
    head_tree = head_commit.tree
    #STDERR.puts "head_tree: #{head_tree.inspect}"
    begin
      paths = repo.git.ls_files(
        ".*#{Regexp.escape(suffix)}$",
        path: templates_dirname,
        ref: head_ref.name
      )
    rescue
      STDERR.puts "No *#{suffix} file under #{
        templates_dirname}/ in repo at ref #{head_ref.name}"
    end
    return <<EOF if paths.nil? || paths.empty?
ERROR: No *#{suffix} page templates defined!
EOF

    #STDERR.puts "paths: #{paths.inspect}"
    props = nil
    data = nil
    name_patterns = paths.map do |path|
      name = File.basename(path)
      #STDERR.puts "name: #{name}"
      name.delete_suffix(suffix).delete_suffix(document_format_suffix)
    end
    name_patterns.each_with_index do |name_pattern, i|
      STDERR.puts "name_pattern: #{name_pattern}"
      regexp_text = "^#{Regexp.escape(name_pattern).gsub(/\\\{\\\+([A-Za-z_]+)\\\}/, '(?<\1>.*?)')}$"
      STDERR.puts "regexp_text: #{regexp_text}"
      regexp = Regexp.new(regexp_text, Regexp::IGNORECASE)
      #STDERR.puts "regexp: #{regexp}"
      matches = regexp.match(
        new_page_file_name.delete_suffix(document_format_suffix)
      )
      STDERR.puts "matches: #{matches}"
      next if matches.nil?

      props = matches.named_captures
      #STDERR.puts "props: #{props}"
      path = paths[i]
      #STDERR.puts "path: #{path}"
      blob = head_tree./(path)
      #STDERR.puts "blob: #{blob.inspect}"
      data = blob.data
      break
    end
    return <<EOF if props.nil?
WARNING: No *#{suffix} page template found matching the new page's name!
New page's name: #{new_page_file_name}
Candidate *#{suffix} page template names: #{name_patterns.map { |p| "\n- #{p}" }.join('')}
N.B: the document format suffix is ignored: #{document_format_suffix}
EOF

    props['YYYY-MM-DD'] = DateTime.now.strftime('%F')
    Mustache.render(data, props)
  end)
end

add_context_aware_content_filter(
  '{{context_aware_content}}',
  '_Templates',
  '.mustache'
)

wiki_options = {
  template_dir: '/templates'
}

Precious::App.set(:wiki_options, wiki_options)
