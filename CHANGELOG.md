Latest changes
====

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

0.0.7 / Unreleased
----

0.0.6 / 2023-05-30
----

* [FEATURE] Implement context-aware (title-driven for now) page templates;
  sorry no documentation yet...
* [BUGFIX] Fix links to tagging/tagged pages with spaces in their name
* [ENHANCEMENT] Sort the index's tagged document paths in \*`..json` files:
  * the document list gets pre-sorted for later display, only
    at a small cost because the indexing is incremental
  * the files are easier to diagnose and modify manually

0.0.5 / 2023-04-10
----

* [FEATURE] Support subtags (including crude cycle protection)
* [BUGFIX] Honor `page_file_dir` setting instead of breaking when set
  to some other place than the Git repository root; also make renames
  and deletions work in that case

0.0.4 / 2023-03-15
----

* [FEATURE] Implement document tags rendering alternatively as macro
* [ENHANCEMENT] Honor `display_metadata` setting for the original 0.0.3's
  document tags rendering (Mustache-template based)
  NOTE: somehow now an empty tag list does not display anything though...

0.0.3 / 2023-02-19
----

* [FEATURE] Render document tags as hyperlinks

0.0.2 / 2023-02-10
----

* [ENHANCEMENT] Separate tagging code from the site-specific Gollum config
* [BUGFIX] Invert backwards structure:
  the content repo was inside the engine repo!

0.0.1 / 2023-02-09
----

* First released version
