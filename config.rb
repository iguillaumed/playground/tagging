# frozen_string_literal: true

require_relative 'lib'

Gollum::Hook.register(:post_commit, :hook_id) do |committer, sha1|
  GollumTagging::post_commit_hook(committer, sha1)
end

wiki_options = {
  display_metadata: false,
  template_page: true
}

Precious::App.set(:wiki_options, wiki_options)
