Tagging system
===============================================================================

Tag syntax
-------------------------------------------------------------------------------

```yaml
---
tags:
---
```

'---' supported by all unlike ending '...'

`tags` property known by at least Jekyll, Hugo, Zettlr, ...

References
-------------------------------------------------------------------------------

 - [Query: Does gollum support backlinks? #1622](https://github.com/gollum/gollum/issues/1622)
 - [Tag Based Organization #1058](https://github.com/gollum/gollum/issues/1058)
 - <https://www.rubydoc.info/gems/rugged/1.1.0/Rugged/Index#each-instance_method>
 - [about tags really:] [Request Params in Macro #997](https://github.com/gollum/gollum/issues/997)
 - <https://docs.zettlr.com/en/core/yaml-frontmatter/>
 - <https://jekyllrb.com/docs/front-matter/>
 - <https://github.blog/2013-09-27-viewing-yaml-metadata-in-your-documents/>
 - GitLab passively supports YAML frontmatter in Wiki: <https://gitlab.com/gitlab-org/gitlab/-/merge_requests/27706> newer? https://docs.gitlab.com/ee/user/markdown.html#front-matter

Gollum is a good choice: https://decovar.dev/blog/2021/01/07/gollum-markdown-wiki/
