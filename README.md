Tagging system
====

Usage
----

- Gollum:
  - clone this repo as `.tagging-engine/` inside your Gollum content
    Git repository's root directory
  - start using `.tagging-engine/start`
    and head over to <http://localhost:4567/>
  - manually add `<<AllPagesTagged(`_OPTIONAL-MAX-TREE-DEPTH_`)>>` on each page
    to insert the hyperlinked tree of tagged pages there
    (it should also work in the sidebar)
  - manually add `<<PageTags()>>` on any/some/all of the aside special pages
    (header, footer, sidebar)
    to insert the current page's hyperlinked list of tags there
    (it does not work on the current page itself for technical reasons)
- all content files should land under `../`
  - add [tags](tags.md) to the Markdown files ___using Gollum___
  - __Important__: pages in __subdirectories are not supported _yet___
